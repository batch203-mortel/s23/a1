let trainer = {};
trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brokc", "Misty"]
	}

}

trainer.talk = function(pokemon){
	if(trainer.pokemon.includes(pokemon)){
		console.log(`${pokemon}! I choose you!`)
	}
	else{
		console.log(`${pokemon} is not on your list yet. Catch one first!` )
	}
	
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk("Squirtle");




